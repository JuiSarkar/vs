﻿using System;

namespace ArraySum
{
    //conflict from main proj
    class Program
    {
        static int[] inputarray;
        static void Main(string[] args)
        {
            Console.WriteLine("Enter size of an array: ");
            int size = int.Parse(Console.ReadLine());
            inputarray = new int[size];

            for(int i=0;i<size;i++)
            {
                Console.WriteLine("Enter the elements of an array:");
                inputarray[i]= int.Parse(Console.ReadLine());
            }
            int result = sum();
            Console.WriteLine("The sum of array is {0} ",result);
            Console.ReadLine();
        }

        static int sum()
        {
            int add = 0;
            for(int i=0;i<inputarray.Length;i++)
            {
                add = add + inputarray[i];
            }
            return add;
        }
    }
}
